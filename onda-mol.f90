

program onda_mol

! This is a simple code for the wave equation

! define:
!
! pi  = dphi/dt
! psi = dphi/dx


! ******************************
! ***   WAVE EQUATION   ***
! ******************************

! Declare variables.

  implicit none

  integer i,j,l         ! Counters
  integer Nt            ! Total number of time steps.
  integer Nx            ! Total number of grid points.
  integer Noutput       ! Frequency of output.

  real(8) dx            ! Grid spacing.
  real(8) dt            ! Time step.
  real(8) t             ! Time.

  real(8) rho       ! courant parameter

  real(8) v             ! Wave speed.

  real(8) x0            ! Center of initial gaussian.
  real(8) s0            ! Width of initial gaussian.
  real(8) a0            ! Amplitude of initial gaussian.

  character(20) method  ! Integration method.
  integer method_flag  ! Integration method.

  character(20) order

  character(20) bound_type

  real(8), allocatable, dimension(:) :: x       ! Position.

  real(8), allocatable, dimension(:) :: phi,phi_p,sphi,D1_phi,phi_aux     ! Wave function.
  real(8), allocatable, dimension(:) :: psi,psi_p,spsi,D1_psi,psi_aux     ! Spatial derivative
  real(8), allocatable, dimension(:) :: pi ,pi_p ,spi ,D1_pi ,pi_aux      ! Time derivative 

  real(8) phi_out,psi_out,pi_out

  character(30) :: out_dir !output directory

  integer icn_maxiter 
  real    icn_omega

! **************************
! ***   GET PARAMETERS   ***
! **************************

  print *
  print *, 'Give grid spacing dx'
  read(*,*) dx

  print *
  print *, 'Give time step dt'
  read(*,*) dt

  print *
  print *, 'Give wave speed'
  read(*,*) v

  print *
  print *, 'Give total number of grid points Nx'
  read(*,*) Nx

  print *
  print *, 'Give total number of time steps'
  read(*,*) Nt

  print *
  print *, 'Give frequency of output'
  read(*,*) Noutput

  print *
  print *, 'Give integration method '
  print *, '1. ICN '

  read(*,*) method_flag

  print *
  print *, 'Spatial differentiation order (two,four)'
  read(*,*) order

  print *
  print *, 'Boundary condition (transparent)'
  read(*,*) bound_type


  print*
  write(*,*) "directorio de salida"
  read(*,*) out_dir


! Make directory
  call system('mkdir -p '//trim(out_dir))


! **************************************
! ***   ALLOCATE MEMORY FOR ARRAYS   ***
! **************************************

  allocate(x(0:Nx))

  allocate(phi(0:Nx),phi_p(0:Nx),sphi(0:Nx),D1_phi(0:Nx),phi_aux(0:Nx))
  allocate(psi(0:Nx),psi_p(0:Nx),spsi(0:Nx),D1_psi(0:Nx),psi_aux(0:Nx))
  allocate(pi(0:Nx) ,pi_p(0:Nx) ,spi(0:Nx) ,D1_pi(0:Nx) ,pi_aux(0:Nx))


! *************************************
! ***   FIND GRID POINT POSITIONS   ***
! *************************************

  do i=0,Nx
     x(i) = dble(i)*dx
  end do


! ****************************
! ***   OUTPUT TO SCREEN   ***
! ****************************

  print *,'------------------------------'
  print *,'|  Time step  |     Time     |'
  print *,'------------------------------'


! ************************
! ***   INITIAL DATA   ***
! ************************

! Initialize time.

  t = 0.0D0

! Initialize courant factor

  rho = dt/dx

! Parameters for initial data.

  a0 = 1.0D0
  x0 = dble(Nx/2)*dx
  s0 = 1.0D0

! Initial data (gaussian).

  do i=0,Nx
     phi(i) = a0*exp(-(x(i)-x0)**2/s0**2)
     pi(i)=0.d0
  end do

  psi = diff1(Nx,dx,phi,order)
  

! Output to screen.

  write(*,"(A5,I7,A5,ES11.4,A4)") ' |   ',0,'   | ',t,'  | '


! *****************************
! ***   OPEN OUTPUT FILES   ***
! *****************************

   open(1,file=trim(out_dir)//'/phi.xl',form='formatted',status='replace')
   open(2,file=trim(out_dir)//'/psi.xl',form='formatted',status='replace')
   open(3,file=trim(out_dir)//'/pi.xl',form='formatted',status='replace')
   open(4,file=trim(out_dir)//'/onda.dat',form='formatted',status='replace')


! *********************************
! ***   SAVE THE INITIAL DATA   ***
! *********************************

! Wave function.

  write(1,"(A8,ES14.6)") '#Time = ',t

  do i=0,Nx
     if (dabs(phi(i)) > 1.0D-50) then
        write(1,"(2ES16.8)") x(i),phi(i)
        phi_out = phi(i)
     else
        write(1,"(2ES16.8)") x(i),0.0D0
        phi_out = 0.d0
     end if
     if (dabs(psi(i)) > 1.0D-50) then
        write(2,"(2ES16.8)") x(i),psi(i)
        psi_out = psi(i)
     else
        write(2,"(2ES16.8)") x(i),0.0D0
        psi_out = 0.d0
     end if
     if (dabs(pi(i)) > 1.0D-50) then
        write(3,"(2ES16.8)") x(i),pi(i)
        pi_out = pi(i)
     else
        write(3,"(2ES16.8)") x(i),0.0D0
        pi_out = 0.d0
     end if

     write(4,"(4ES16.8)") x(i),phi_out,psi_out,pi_out
     
  end do


! Leave blank spaces in each putput file before next time level.

  write(1,*)
  write(1,*)
  write(2,*)
  write(2,*)
  write(3,*)
  write(3,*)
  write(4,*)
  write(4,*)


! *************************************
! ***   START MAIN EVOLUTION LOOP   ***
! *************************************

  icn_maxiter = 3

  do l=1,Nt

!    Time.

     t = t + dt

!    Save old time step values.

     phi_p = phi
     psi_p = psi
     pi_p  = pi

     if (method_flag==1) then 
        ! Iterative Crank-Nicholson
       
        do i = 1, icn_maxiter
           ! Evaluate derivatives
           D1_psi = diff1(Nx,dx,psi,order)
           D1_pi  = diff1(Nx,dx,pi,order)

           ! Evaluate sources
           sphi = pi
           spsi = D1_pi
           spi  = v**2*D1_psi

           ! Boundaries

           if(bound_type=="transparent") then

              ! The sources at the boundary are evaluated taking sided
              ! differences. This is not stable since the incoming
              ! modes are not evaluated in terms of their dependence
              ! domain.

              ! Right boundary
              ! the incoming mode is pi + v*psi and we set it equal to zero
              ! then, differentiating
              !
              !  d pi = - v*d psi
              !   t          t

              spi(Nx)= -v*spsi(Nx)

              ! Left boundary
              ! the incoming mode is pi - v*psi and we set it equal to zero
              ! then, differentiating
              !
              !  d pi =  v*d psi
              !   t         t

              spi(0)= v*spsi(0)

           else

              if(l==1) then
                 print *, 'Unknown boundary condition.'
                 print *
              end if

           end if

           ! Update
           if(i<icn_maxiter) then
              phi = phi_p +0.5d0*dt*sphi
              psi = psi_p +0.5d0*dt*spsi
              pi  = pi_p  +0.5d0*dt*spi
           else
              phi = phi_p +dt*sphi
              psi = psi_p +dt*spsi
              pi  = pi_p  +dt*spi 
           end if

        end do

     elseif (method_flag==2) then 
        ! RK4

        print *, 'Runge-Kutta not yet implemented'
        print *
        stop

     else

        print *, 'Unknown integartion method.'
        print *
        stop

     end if


!    *****************************
!    ***   SAVE DATA TO FILE   ***
!    *****************************

     if (mod(l,Noutput).eq.0) then

        write(1,"(A8,ES14.6)") '#Time = ',t


        do i=0,Nx
           if (dabs(phi(i)) > 1.0D-50) then
              write(1,"(2ES16.8)") x(i),phi(i)
              phi_out = phi(i)
           else
              write(1,"(2ES16.8)") x(i),0.0D0
              phi_out = 0.d0
           end if
           if (dabs(psi(i)) > 1.0D-50) then
              write(2,"(2ES16.8)") x(i),psi(i)
              psi_out = psi(i)
           else
              write(2,"(2ES16.8)") x(i),0.0D0
              psi_out = 0.d0
           end if
           if (dabs(pi(i)) > 1.0D-50) then
              write(3,"(2ES16.8)") x(i),pi(i)
              pi_out = pi(i)
           else
              write(3,"(2ES16.8)") x(i),0.0D0
              pi_out = 0.d0
           end if
           
           write(4,"(4ES16.8)") x(i),phi_out,psi_out,pi_out
           
        end do


! Leave blank spaces before next time level.

        write(1,*)
        write(1,*)

        write(2,*)
        write(2,*)
        write(3,*)
        write(3,*)
        write(4,*)
        write(4,*)


     end if


!    ***********************************
!    ***   END MAIN EVOLUTION LOOP   ***
!    ***********************************

!    Time step information to screen.

     if (mod(l,Noutput).eq.0) then
        write(*,"(A5,I7,A5,ES11.4,A4)") ' |   ',l,'   | ',t,'  | '
     end if

  end do

  print *,'------------------------------'


! ******************************
! ***   CLOSE OUTPUT FILES   ***
! ******************************

  close(1)
  close(2)
  close(3)
  close(4)


! ***************
! ***   END   ***
! ***************

  print *
  print *, 'PROGRAM HAS FINISHED'
  print *
  print *, 'Have a nice day!'
  print *
  print *
  print *


contains 

  function diff1(Nx,dx,var,order)

! *****************************************************
! ***   CALCULATE FIRST DERIVATIVE TO GIVEN ORDER   ***
! *****************************************************

    implicit none

    integer i,Nx

    real(8) dx,idx,hidx

    real(8), dimension (0:Nx) :: var,diff1

    character(len=*) order


! *******************
! ***   NUMBERS   ***
! *******************

    idx  = 1.d0/dx
    hidx = 0.5d0*idx


! ************************
! ***   SECOND ORDER   ***
! ************************

    if (trim(order)=="two") then

!    Interior points: second order centered first derivative.

       do i=1,Nx-1
          diff1(i) = hidx*(var(i+1) - var(i-1))
       end do

!    Last point and first point with second order one-sided first derivative.

       diff1(Nx) = hidx*(3.d0*var(Nx) - 4.d0*var(Nx-1) + var(Nx-2))
       diff1(0) = - hidx*(3.d0*var(0) - 4.d0*var(1) + var(2))



! ************************
! ***   FOURTH ORDER   ***
! ************************

    else if (trim(order)=="four") then

!    Interior points: fourth order centered first derivative.

       do i=2,Nx-2
          diff1(i) = 0.25d0*idx*(8.d0*(var(i+1) - var(i-1)) &
               - (var(i+2) - var(i-2)))/3.d0
       end do

!    Last-but-one point with fourth order semi-one-sided first derivative.

       diff1(Nx-1) = 0.25d0*idx*(3.d0*var(Nx) + 10.d0*var(Nx-1) &
            - 18.d0*var(Nx-2) + 6.d0*var(Nx-3) - var(Nx-4))/3.d0
       
       diff1(1) = -(0.25d0*idx*(3.d0*var(0) + 10.d0*var(1) &
            - 18.d0*var(2) + 6.d0*var(3) - var(Nx-4))/3.d0)

!    Last point with fourth order one-sided first derivative.

       diff1(Nx) = 0.25d0*idx*(25.d0*var(Nx) - 48.d0*var(Nx-1) &
            + 36.d0*var(Nx-2) - 16.d0*var(Nx-3) + 3.d0*var(Nx-4))/3.d0
       
       diff1(0) = -(0.25d0*idx*(25.d0*var(0) - 48.d0*var(1) &
            + 36.d0*var(2) - 16.d0*var(3) + 3.d0*var(4))/3.d0)

    end if

  end function diff1



end program onda_mol


